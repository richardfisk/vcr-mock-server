require 'rubygems'
require 'test/unit'
require 'vcr'

VCR.configure do |config|
  config.cassette_library_dir = "fixtures/vcr_cassettes"
  config.hook_into :webmock # or :fakeweb
end

class VCRTest < Test::Unit::TestCase
  def test_example_dot_com
    VCR.use_cassette("job_detail") do
      #THIS IS A LOCAL REQUEST
      response = Net::HTTP.get_response(URI('http://m.seek.com.au//api/v2/mobile/jobdetails/28102822'))
      assert_match /Example domains/, response.body
    end
  end
end