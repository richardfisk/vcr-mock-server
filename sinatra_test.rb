# require_relative 'vcr/spec/support/cucumber_helpers.rb'
require 'sinatra'
require 'rspec'
require 'vcr'
require_relative 'vcr/spec/support/vcr_localhost_server'

def start_sinatra_app(options, &block)
  raise ArgumentError.new("You must pass a port") unless options[:port]

  klass = Class.new(Sinatra::Base)
  klass.disable :protection
  klass.class_eval(&block)

  VCR::LocalhostServer.new(klass.new, options[:port])
end

def get_http_headers request_env
    request_headers = {}

    request_env.each do |headerKey, headerValue|
        is_http_header = headerKey.start_with? "HTTP_"

        if is_http_header
            request_headers[headerKey] = headerValue
        end
    end

    request_headers
end

start_sinatra_app({:port => 7777}) do

    VCR.configure do |c|
        c.ignore_request do |request|
            URI(request.uri).path == "/__identify__"
        end
        c.cassette_library_dir = "fixtures/vcr_cassettes"
        c.default_cassette_options = { :record => :once, :allow_playback_repeats => true }
        c.hook_into :fakeweb
    end

    get '/' do
        request_headers = get_http_headers request.env
        
        http = Net::HTTP.new(nox_url.host, nox_url.port)
        http.use_ssl = nox_url.scheme == "https"
        method = request.env["HTTP_NOX_METHOD"]
        puts "*** VCR #{method} request: #{nox_url}"

        resp = nil

        VCR.use_cassette('test1', :record => :once) do
            
            request_headers["HTTP_HOST"] = request_headers["HTTP_NOX_URL"]
            
            request = Net::HTTP::Get.new(nox_url)

            request_headers.each do |key, value|
                stripped_key = key.sub "HTTP_", ""
                request.add_field(stripped_key, request_headers[key]) if key
            end
            # proxy_addr = "localhost"
            # proxy_port = "8888"
            # Net::HTTP.new(nox_url.host, nil, proxy_addr, proxy_port).start { |http|
              # always proxy via your.proxy.addr:8080
                resp = http.request(request)
            # }
            puts "Response", resp
        end


        resp.each do |header|
            headers[header] = resp[header]
        end

        status resp.code
        body resp.body
    end

    def nox_url
        @url ||= URI.parse(request.env['HTTP_NOX_URL'])
    end

end
